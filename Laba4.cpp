#include "pch.h" 
#include <math.h> 
#include <stdio.h> 

int main()
{
	float a, x, z, n, step, min, max;
	float G[10000], F[10000], Y[10000];
	int b;
	printf("--------------------------------------------------------------------- \n");
	printf("Введите значение переменной a=");
	scanf_s("%f", &a);
	printf("Введите нальное значение переменной x=");
	scanf_s("%f", &x);
	printf("Введите конечное значение переменной x=");
	scanf_s("%f", &z);
	printf("Введите количество шагов расчета функций n=");
	scanf_s("%f", &n);
	printf("Введите номер формулы (G-1, F-2,Y-3):");
	scanf_s("%d", &b);
	printf("---------------------------------------------------------------------\n");
	step = (z - x) / n;
	switch (b) {
	case 1:
		for (int i = 0; i < n; i++) {
			if (((-1) * pow(a, 2) + a * x + 6 * pow(x, 2)) != 0) {
				G[i] = (-10 * (18 * pow(a, 2) + 11 * a * x - 24 * pow(x, 2))) / ((-1) * pow(a, 2) + a * x + 6 * pow(x, 2));
				printf("x=%f a=%f G=%f \n", x, a, G[i]);
			}
			else {
				printf("Не возможно произверсти рассчёт функции G \n");
				G[i] = 0;
			}
			if (i == 0) {
				min = G[i];
				max = G[i];
			}
			else {
				if (min > G[i]) {
					min = G[i];
				}
				if (max < G[i]) {
					max = G[i];
				}
			}
			x += step;
		}
		printf("--------------------------------------------------------------------- \n");
		printf("Минимальное значение функции =%f \n", min);
		printf("Максимальное значение функции =%f \n", max);
		break;
	case 2:
		for (int i = 0; i < n; i++) {
			F[i] = cosh(21 * pow(a, 2) - 34 * a * x + 9 * pow(x, 2));
			printf("x=%f a=%f F=%f \n ", x, a, F[i]);

			if (i == 0) {
				min = F[i];
				max = F[i];
			}
			else {
				if (min > F[i]) {
					min = F[i];
				}
				if (max < F[i]) {
					max = F[i];
				}
			}
			x += step;
		}
		printf("--------------------------------------------------------------------- \n");
		printf("Минимальное значение функции =%f \n", min);
		printf("Максимальное значение функции =%f \n", max);
		break;
	case 3:
		for (int i = 0; i < n; i++) {
			if ((3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1) >= 0) {
				Y[i] = (log(3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1)) / log(10);
				printf("x=%f a=%f Y=%f \n", x, a, Y[i]);
			}
			else {
				printf("Не возможно произверсти рассчёт функции Y \n");
				Y[i] = 0;
			}

			if (i == 0) {
				min = Y[i];
				max = Y[i];
			}
			else {
				if (min > Y[i]) {
					min = Y[i];
				}
				if (max < Y[i]) {
					max = Y[i];
				}
			}
			x += step;
		}
		printf("--------------------------------------------------------------------- \n");
		printf("Минимальное значение функции =%f \n", min);
		printf("Максимальное значение функции =%f \n", max);
		break;

	default:
		printf("Не верно введён номер функции \n");
	}
	printf("--------------------------------------------------------------------- \n");

	return 0;
}