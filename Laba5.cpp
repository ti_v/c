#include "pch.h" 
#include <math.h> 
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 

int main()
{
	float a, x, z, n, step, G, F, Y, result[1024];
	char a_s[255], x_s[255], z_s[255], n_s[255], result_s[1024], pattern[1024];
	int b, l = 0, v = 1, coincidence = 0;
	char *istr;
	printf("------------------------------------------------------------------- \n");
	printf("Введите значение переменной a=");
	scanf("%s", &a_s);
	printf("Введите нальное значение переменной x=");
	scanf("%s", &x_s);
	printf("Введите конечное значение переменной x=");
	scanf("%s", &z_s);
	printf("Введите количество шагов расчета функций n=");
	scanf("%s", &n_s);
	printf("Введите номер формулы (G-1, F-2,Y-3):");
	scanf("%d", &b);
	printf("---------------------------------------------------------------------\n");

	a = atof(a_s);
	x = atof(x_s);
	z = atof(z_s);
	n = atof(n_s);
	step = (z - x) / n;
	sprintf_s(result_s, " ");

	switch (b) {
	case 1:
		l = 1;
		for (int i = 0; i < n; i++) {
			if (((-1) * pow(a, 2) + a * x + 6 * pow(x, 2)) != 0) {
				result[i] = (-10 * (18 * pow(a, 2) + 11 * a * x - 24 * pow(x, 2))) / ((-1) * pow(a, 2) + a * x + 6 * pow(x, 2));
				sprintf(result_s, "%s %f", result_s, result[i]);
			}
			x += step;
		}
		break;
	case 2:
		l = 1;
		for (int i = 0; i < n; i++) {
			result[i] = cosh(21 * pow(a, 2) - 34 * a * x + 9 * pow(x, 2));
			sprintf(result_s, "%s %f", result_s, result[i]);
			x += step;
		}
		break;
	case 3:
		l = 1;
		for (int i = 0; i < n; i++) {
			if ((3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1) >= 0) {
				result[i] = (log(3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1)) / log(10);
				sprintf(result_s, "%s %f", result_s, result[i]);
			}
			x += step;
		}
		break;
	default:
		printf("Не верно введён номер функции \n");
	}
	if (l == 1) {
		printf("Результаты вычилений %s \n", result_s);
		printf("Введите шаблон:");
		scanf("%s", &pattern);

		while (true) {
			istr = strstr(result_s + v, pattern);

			if (istr == NULL) {
				break;
			}
			v = istr - result_s + 1;
			coincidence++;

		}

		printf("Количество совпадений = %d \n", coincidence);
	}

	return 0;
}