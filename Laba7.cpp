#include "pch.h" 
#include <math.h> 
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include <io.h>
#include <fcntl.h>
#include <locale.h>

struct Resultat {
	char Func;
	float a, x, Rez;
};

int main()
{
	float a, x, z, n, step;
	struct Resultat result[1024];
	char a_s[255], x_s[255], z_s[255], n_s[255];
	int l = 0;
	char filename[] = "text.txt";
	FILE *file;

	if ((file = fopen(filename, "w")) == NULL)
	{
		printf("Ошибка при открытии файла.\n");
		return 0;
	}
	printf("-----------------------------------------------------------------------------------\n");
	printf("Введите значение переменной a=");
	scanf("%s", &a_s);
	printf("Введите нальное значение переменной x=");
	scanf("%s", &x_s);
	printf("Введите конечное значение переменной x=");
	scanf("%s", &z_s);
	printf("Введите количество шагов расчета функций n=");
	scanf("%s", &n_s);
	printf("-----------------------------------------------------------------------------------\n");

	a = atof(a_s);
	x = atof(x_s);
	z = atof(z_s);
	n = atof(n_s);
	step = (z - x) / n;
	printf("\tFunc\t|\tX           \t|\tA           \t|\tRez\n");
	printf("-----------------------------------------------------------------------------------\n");
	for (int i = 0; i < n; i++) {
		if (((-1) * pow(a, 2) + a * x + 6 * pow(x, 2)) != 0) {
			result[l].Func = 'G';
			result[l].x = x;
			result[l].a = a;
			result[l].Rez = (-10 * (18 * pow(a, 2) + 11 * a * x - 24 * pow(x, 2))) / ((-1) * pow(a, 2) + a * x + 6 * pow(x, 2));
			//fprintf(file, "\t%c\t|\t%f\t|\t%f\t|\t%f\n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
			fprintf(file, "%c %f %f %f \n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
			l++;
		}

		result[l].Func = 'F';
		result[l].x = x;
		result[l].a = a;
		result[l].Rez = cosh(21 * pow(a, 2) - 34 * a * x + 9 * pow(x, 2));
		fprintf(file, "%c %f %f %f \n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
		l++;

		if ((3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1) >= 0) {
			result[l].Func = 'Y';
			result[l].x = x;
			result[l].a = a;
			result[l].Rez = (log(3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1)) / log(10);
			fprintf(file, "%c %f %f %f \n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
			l++;
		}

		x += step;
	}
	fclose(file);
	memset(result, 0, 1024 * (sizeof result[0]));
	if ((file = fopen(filename, "r+")) == NULL)
	{
		printf("Ошибка при открытии файла.\n");
		return 0;
	}
	int t = 0;
	for (int i = 0; i < l; i++) {
		fscanf(file, "%c %f %f %f /n", &result[i].Func, &result[i].x, &result[i].a, &result[i].Rez);
		printf("\t%c\t|\t%f\t|\t%f\t|\t%f\n", result[i].Func, result[i].x, result[i].a, result[i].Rez);
	}
	fclose(file);
	return 0;
}