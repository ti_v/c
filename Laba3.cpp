#include "pch.h" 
#include <math.h> 
#include <stdio.h> 

int main()
{
	float a, x, z, n, step;
	int b;
	printf("---------------------------------------------------------------------\n");
	printf("Введите значение переменной a=");
	scanf_s("%f", &a);
	printf("Введите нальное значение переменной x=");
	scanf_s("%f", &x);
	printf("Введите конечное значение переменной x=");
	scanf_s("%f", &z);
	printf("Введите количество шагов расчета функций n=");
	scanf_s("%f", &n);
	printf("Введите номер формулы (G-1, F-2,Y-3):");
	scanf_s("%d", &b);
	printf("---------------------------------------------------------------------\n");
	step = (z - x) / n;
	switch (b) {
	case 1:
		for (int i = 0; i < n; i++) {
			if (((-1) * pow(a, 2) + a * x + 6 * pow(x, 2)) != 0)
			{
				printf("x=%f a=%f G=%f\n", x, a, (-10 * (18 * pow(a, 2) + 11 * a * x - 24 * pow(x, 2))) / ((-1) * pow(a, 2) + a * x + 6 * pow(x, 2)));
			}
			else {
				printf("Не возможно произверсти рассчёт функции G\n");
			}
			x += step;
		}
		break;
	case 2:
		for (int i = 0; i < n; i++) {
			printf("x=%f a=%f F=%f\n", x, a, cosh(21 * pow(a, 2) - 34 * a * x + 9 * pow(x, 2)));
			x += step;
		}
		break;
	case 3:
		for (int i = 0; i < n; i++) {
			if ((3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1) >= 0) {
				printf("x=%f a=%f Y=%f\n", x, a, (log(3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1)) / log(10));
			}
			else {
				printf("Не возможно произверсти рассчёт функции Y\n");
			}
			x += step;
		}
		break;
	default:
		printf("Не верно введён номер функции\n");
	}
	printf("---------------------------------------------------------------------\n");
	return 0;
}