#include "pch.h" 
#include <math.h> 
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 

struct Resultat {
	char Func;
	float a, x, Rez;
};

int main()
{
	float a, x, z, n, step;
	struct Resultat result[1024];
	char a_s[255], x_s[255], z_s[255], n_s[255];
	int l = 0;

	printf("-----------------------------------------------------------------------------------\n");
	printf("Введите значение переменной a=");
	scanf("%s", &a_s);
	printf("Введите нальное значение переменной x=");
	scanf("%s", &x_s);
	printf("Введите конечное значение переменной x=");
	scanf("%s", &z_s);
	printf("Введите количество шагов расчета функций n=");
	scanf("%s", &n_s);
	printf("-----------------------------------------------------------------------------------\n");

	a = atof(a_s);
	x = atof(x_s);
	z = atof(z_s);
	n = atof(n_s);
	step = (z - x) / n;
	printf("\tFunc\t|\tX           \t|\tA           \t|\tRez\n");
	printf("-----------------------------------------------------------------------------------\n");
	for (int i = 0; i < n; i++) {
		if (((-1) * pow(a, 2) + a * x + 6 * pow(x, 2)) != 0) {
			result[l].Func = 'G';
			result[l].x = x;
			result[l].a = a;
			result[l].Rez = (-10 * (18 * pow(a, 2) + 11 * a * x - 24 * pow(x, 2))) / ((-1) * pow(a, 2) + a * x + 6 * pow(x, 2));
			printf("\t%c\t|\t%f\t|\t%f\t|\t%f\n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
			l++;
		}
		result[l].Func = 'F';
		result[l].x = x;
		result[l].a = a;
		result[l].Rez = cosh(21 * pow(a, 2) - 34 * a * x + 9 * pow(x, 2));
		printf("\t%c\t|\t%f\t|\t%f\t|\t%f\n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
		l++;

		if ((3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1) >= 0) {
			result[l].Func = 'Y';
			result[l].x = x;
			result[l].a = a;
			result[l].Rez = (log(3 * pow(a, 2) - 25 * a*x + 8 * pow(x, 2) + 1)) / log(10);
			printf("\t%c\t|\t%f\t|\t%f\t|\t%f\n", result[l].Func, result[l].x, result[l].a, result[l].Rez);
			l++;
		}
		x += step;
	}
	printf("-----------------------------------------------------------------------------------\n");
	return 0;
}